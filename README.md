## Setup

- docker-compose exec graphavel_db bash
- mysql -u root -p
- GRANT ALL ON graphavel.* TO 'graphavel'@'%' IDENTIFIED BY 'graphavel_pwd';
- FLUSH PRIVILEGES;
- EXIT;

- docker-compose up
- docker-compose exec graphavel_app cp .env.example .env
- docker-compose exec graphavel_app php artisan key:generate
- docker-compose exec graphavel_app php artisan config:cache
- docker-compose exec graphavel_app composer install
- docker-compose exec graphavel_app php artisan passport:install

mutation {
  login(input: {
    username: "private@email.com",
    password: "complex_password"
  }) {
    access_token
    refresh_token
    expires_in
    token_type
    user {
      id
      email
      name
    }
  }
}
