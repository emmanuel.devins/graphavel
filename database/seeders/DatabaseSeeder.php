<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();

        // User to test passport 😁
        \App\Models\User::create([
            'name' => 'Emmanuel Devins',
            'email' => 'private@email.com',
            'password' => bcrypt('complex_password')
        ]);
    }
}
